<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::resource('perfil', 'PerfilController')->middleware('auth');
Route::resource('playlist', 'PlaylistController')->middleware('auth');
Route::resource('video', 'VideoController')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home');
