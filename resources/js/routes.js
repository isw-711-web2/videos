import Dashboard from './components/admin/DashboardComponent.vue'
// import Profile from './components/admin/ProfileComponent.vue'
import User from './components/admin/UserComponent.vue'
import Perfil from './components/admin/PerfilComponent.vue'
import Playlist from './components/admin/PlaylistComponent.vue'
import Youtube from './components/admin/YoutubeComponent.vue'
import Video from './components/admin/VideoComponent.vue'

export const routes = [
    {
        path:'/dashboard',
        component:Dashboard
    },
    // {
    //     path:'/profile',
    //     component:Profile
    // },
    { 
        path:'/users',
        component:User
    },
    { 
        path:'/perfils',
        component:Perfil
    },
    { 
        path:'/playlists',
        component:Playlist
    },
    { 
        path:'/youtube',
        component:Youtube
    },
    { 
        path:'/video',
        component:Video
    },
 
 
];