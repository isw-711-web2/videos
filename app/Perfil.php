<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Perfil extends Model
{
    
    use Notifiable;

    protected $fillable = [
        'full_name', 'username', 'pin',
    ];
}
