<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Playlist;

class PlaylistController extends Controller
{
    public function index(Request $request)
    {
        // $now = Carbon::now();
        
         if($request->wantsJson()){
         return Playlist::where('user_id', auth()->id())->get();
         }
    }

        public function store(Request $request)
        {   
            $this->validate($request, [
                'name' => 'required',
                'url' => 'required', 'url'
            ]);
            
            // return Playlist::create([
            //    'name' => $request['name'],
            //    'url' => $request['url'],
            //   'user_id' ->user_id = auth()->id();
            // ]);

            $playlist = new Playlist();
            $playlist->name = $request->name;
            $playlist->url = $request->url;
            $playlist->user_id = auth()->id();
            $playlist->save();
        
            return $playlist;
        }

    public function update(Request $request, $id)
    {   
        // $this->validate($request, [
        //     'name' => 'name',
        //     'url' => 'required',
        //  ]);

        //  $perfil = Perfil::findOrFail($id);

        //  $perfil->update($request->all());

        $playlist = Playlist::find($id);
        $playlist->name = $request->name;
        $playlist->url = $request->url;
        $playlist->save();
        
        return $playlist;
    }

    public function destroy($id)
    {
        $playlist = Playlist::findOrFail($id);
        $playlist->delete();
        return response()->json([
         'message' => 'Playlist deleted successfully', 'status', 200
        ]);
    }
}
