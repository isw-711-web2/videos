<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Perfil;

class PerfilController extends Controller
{
    public function index(Request $request)
    {
         return Perfil::latest()->get();
        
        // if($request->wantsJson()){
        // return Perfil::where('user_id', auth()->id())->get();
        // }
    }

    public function store(Request $request)
    {   
        // $this->validate($request, [
        //     'full_name' => 'required',
        //     'username' => 'required',
        //     'pin' => 'required',
            
        // ]);

        // return Perfil::create([
        //    'full_name' => $request['full_name'],
        //    'username' => $request['username'],
        //    'pin' =>    $request['pin'],
           
        // ]);

        $perfil = new Perfil();
        $perfil->full_name = $request->full_name;
        $perfil->username = $request->username;
        $perfil->pin = $request->pin;
        // $perfil->user_id = auth()->id();
        $perfil->save();
    
        return $perfil;


    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {   
        // $this->validate($request, [
        //     'full_name' => 'required',
        //     'username' => 'required',
        //     'pin' => 'required',
            
        // ]);

        // $perfil = Perfil::findOrFail($id);

        // $perfil->update($request->all());

        $perfil = Perfil::find($id);
        $perfil->full_name = $request->full_name;
        $perfil->username = $request->username;
        $perfil->pin = $request->pin;
        $perfil->save();
        
        return $perfil;
    }

    public function destroy($id)
    {
        $perfil = Perfil::findOrFail($id);
        $perfil->delete();
        return response()->json([
         'message' => 'User deleted successfully'
        ]);
    }
}
