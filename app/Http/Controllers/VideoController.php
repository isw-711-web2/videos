<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Playlist;

class VideoController extends Controller
{
    public function index(Request $request)
    {
        
         if($request->wantsJson()){
         return Playlist::where('user_id', auth()->id())->get();
         }
    }
}
